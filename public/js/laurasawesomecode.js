function callBGG(endpoint, params){
    if(endpoint !== "" && params !== "") {
        var paramstring = "";
        $.each(params, function(key, value){
            paramstring = paramstring + key + "=" + value + "&";
        });

        paramstring = paramstring + "t=" + Math.random();

        $.getJSON("http://bgg-api.herokuapp.com/api/v1/"+endpoint+"?" + paramstring, function(data){
            if(data.message){
                setTimeout(function(){
                    callBGG(endpoint, params);
                }, 500);
            } else {
                return data;
            }
        });
    } else {
        return false;
    }
}

$(document).ready(function() {

    $modal = $('.bgg-modal');

    function showBoardGame(bgname, bgimage, bgyear) {
        $modal.find("h2 span").text(bgname);
        $modal.find("img").attr('src', 'http:' + bgimage);
        $modal.find("h2 small").text(bgyear);
        $modal.modal("show");
    }
    $usererror = $('.nousererror');


    function checktextboxmycollection() {
        username = $('.bggunenter').val();
        if (username == "") {
            $usererror.show();
        } else {
            collectionAPICall();
        }
    };

    function checktextboxhotlist() {
        username = $('.bggunenter').val();
        if (username == "") {
            $usererror.show();
        } else {
            hotlistandusercall();
        }
    };

    function getGameFromMyCollection($itemlist) {
        var totalitems = $itemlist.attr('totalitems');
        $itemlist.randomize();
        var $item = $itemlist.find("item:eq(0)");
        var name = $item.find("name").text();
        var image = $item.find("thumbnail").text();
        var year = $item.find("yearpublished").text();
        showBoardGame(name, image, year);
    }

    function collectionAPICall() {
        username = $('.bggunenter').val();
        $.ajax({
                method: "GET",
                url: "http://www.boardgamegeek.com/xmlapi2/collection?own=1&username=" + username
            })
            .done(function(mylist) {
                $xml = $(mylist).find('items');
                getGameFromMyCollection($xml);
            });
    }

    function hotlistandusercall() {
        username = $('.bggunenter').val();
        $.ajax({
                method: "GET",
                url: "http://www.boardgamegeek.com/xmlapi2/collection?own=1&username=" + username
            })
            .done(function(mylist) {
                $mylist = $(mylist);
                objectids = $mylist.find('item').map(function() {
                    return $(this).attr('objectid')
                }).get();

                $.ajax({
                        method: "GET",
                        url: "http://www.boardgamegeek.com/xmlapi2/hot?type=boardgame"
                    })
                    .done(function(hot) {
                        $xml = $(hot);
                        $hotlist = $xml.find('items').randomize();
                        $hotlist.each(function(index, item) {
                            $item = $(item);
                            if ($.inArray($item.attr('id'), objectids) < 0) {
                                name = $item.find('name').attr('value');
                                year = $item.find('yearpublished').attr("value");
                                image = $item.find('thumbnail').attr("value");
                                showBoardGame(name, image, year);
                                return false;
                            }
                        });
                    });
            });
    };
    $('.massivebuttonplay').on("click", function() {
        buttonclicked = "my collection";
        checktextboxmycollection();
    });

    $('.massivebuttonshow').on("click", function() {
        buttonclicked = "hotlist"
        checktextboxhotlist();
    });

    $('.spinagain').on("click", function() {
        if (buttonclicked === "my collection") {
            collectionAPICall()
        } else {
            hotlistandusercall();
        };
    });


    $.fn.randomize = function(selector) {
        var $elems = selector ? $(this).find(selector) : $(this).children(),
            $parents = $elems.parent();

        $parents.each(function() {
            $(this).children(selector).sort(function() {
                return Math.round(Math.random()) - 0.5;
            }).detach().appendTo(this);
        });

        return this;
    };
});
